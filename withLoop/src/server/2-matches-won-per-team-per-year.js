const csvToJSON = require('../util');
const fs = require('fs');

const matchesFilePath = '../data/matches.csv';

function matchesWonPerTeamPerYear() {
    csvToJSON(matchesFilePath).then((data) => {
        const matchesWon = {};

        for (let i = 0; i < data.length; i++) {
            const match = data[i];
            const year = match['season'];
            const winner = match['winner'];

            if (winner) {
                if (!matchesWon[year]) {
                    matchesWon[year] = {};
                }
                if (!matchesWon[year][winner]) {
                    matchesWon[year][winner] = 0;
                }
                matchesWon[year][winner]++;
            }
        }

        console.log("File executed");
        fs.writeFileSync("../public/output/2-matchesWonPerTeamPerYear.json", JSON.stringify(matchesWon, null, 2));
    }).catch(error => {
        console.error('Error:', error);
    });
}
matchesWonPerTeamPerYear();