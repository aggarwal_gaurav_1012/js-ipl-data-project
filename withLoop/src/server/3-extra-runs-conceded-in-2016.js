const csvToJSON = require('../util');
const fs = require('fs');

const matchesFilePath = '../data/matches.csv';
const deliveriesFilePath = '../data/deliveries.csv';

function extraRunsPerTeam() {
    csvToJSON(matchesFilePath).then((data) => {
    const matchesData = fs.readFileSync(matchesFilePath, 'utf-8');
    const matches = matchesData.split('\n').slice(1);

    const deliveriesData = fs.readFileSync(deliveriesFilePath, 'utf-8');
    const deliveries = deliveriesData.split('\n').slice(1); 

    const matchesPerYear = {};
    const extraRunsPerTeam = {};

    for (let i = 0; i < matches.length; i++) {
        const matchColumns = matches[i].split(',');
        const season = matchColumns[1];
        if (season === '2016') {
            const matchId = matchColumns[0];
            matchesPerYear[matchId] = matchColumns[5];
        }
    }

    for (let i = 0; i < deliveries.length; i++) {
        const deliveryColumns = deliveries[i].split(',');
        const matchId = deliveryColumns[0];
        const bowlingTeam = deliveryColumns[3];
        const extraRuns = parseInt(deliveryColumns[16], 10);

        if (matchesPerYear[matchId] && matchesPerYear[matchId] !== '') {
            const winner = matchesPerYear[matchId];
            if (!extraRunsPerTeam[bowlingTeam]) {
                extraRunsPerTeam[bowlingTeam] = 0;
            }
            if (winner !== bowlingTeam) {
                extraRunsPerTeam[bowlingTeam] += extraRuns;
            }
        }
    }

    console.log("File executed");
    fs.writeFileSync("../public/output/3-extraRunsConcededIn2016.json", JSON.stringify(extraRunsPerTeam, null, 2));
    }).catch(error => {
        console.error('Error:', error);
    });
};
extraRunsPerTeam();