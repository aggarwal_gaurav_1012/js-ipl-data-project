const csvToJSON = require('../util');
const fs = require('fs');

const deliveryFilePath = '../data/deliveries.csv';

function findHighestDismissals() {
    csvToJSON(deliveryFilePath)
        .then((deliveriesData) => {
            const dismissalsCount = {};

            for (let i = 0; i < deliveriesData.length; i++) {
                const dismissalPlayer = deliveriesData[i].player_dismissed;
                const dismissedByPlayer = deliveriesData[i].bowler;

                if (dismissalPlayer !== '' && dismissedByPlayer !== '') {
                    const key = dismissalPlayer + ' dismissed by ' + dismissedByPlayer;
                    dismissalsCount[key] = (dismissalsCount[key] || 0) + 1;
                }
            }

            let maxDismissals = 0;
            let maxDismissalsPair = '';

            for (const pair in dismissalsCount) {
                if (dismissalsCount[pair] > maxDismissals) {
                    maxDismissals = dismissalsCount[pair];
                    maxDismissalsPair = pair;
                }
            }

            console.log("File executed");

            fs.writeFileSync("../public/output/8-playerDismissedByAnother.json", JSON.stringify(dismissalsCount, null, 2));
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

findHighestDismissals();