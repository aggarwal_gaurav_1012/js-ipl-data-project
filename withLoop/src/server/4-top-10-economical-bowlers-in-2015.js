const csvToJSON = require('../util');
const fs = require('fs');

const matchesFilePath = '../data/matches.csv';
const deliveriesFilePath = '../data/deliveries.csv';

function top10EconomicalBowlers(year) {
    csvToJSON(matchesFilePath).then((matchesData) => {

        const matchesOfYear = [];
        for (let i = 0; i < matchesData.length; i++) {
            if (matchesData[i]['season'] === year) {
                matchesOfYear.push(matchesData[i]);
            }
        }

        const matchesOfYearIds = [];
        for (let i = 0; i < matchesOfYear.length; i++) {
            matchesOfYearIds.push(matchesOfYear[i]['id']);
        }


        csvToJSON(deliveriesFilePath).then((deliveriesData) => {
            const bowlers = {};
            const economyRates = {};

            for (let i = 0; i < deliveriesData.length; i++) {
                const delivery = deliveriesData[i];
                const matchId = delivery['match_id'];
                if (matchesOfYearIds.includes(matchId)) {
                    const bowler = delivery['bowler'];
                    const totalRuns = parseInt(delivery['total_runs'], 10);
                    const balls = delivery['wide_runs'] === '0' && delivery['noball_runs'] === '0' ? 1 : 0;

                    if (!bowlers[bowler]) {
                        bowlers[bowler] = { runs: 0, balls: 0 };
                    }

                    bowlers[bowler].runs += totalRuns;
                    bowlers[bowler].balls += balls;
                }
            }

            for (const bowler in bowlers) {
                const runs = bowlers[bowler].runs;
                const balls = bowlers[bowler].balls;
                economyRates[bowler] = (runs / balls) * 6;
            }

            const sortedBowlers = Object.keys(economyRates).sort((a, b) => economyRates[a] - economyRates[b]).slice(0, 10);

            const top10EconomicalBowlers = {};
            for (let i = 0; i < sortedBowlers.length; i++) {
                const bowler = sortedBowlers[i];
                top10EconomicalBowlers[bowler] = economyRates[bowler];
            }
            
            console.log("File executed");
            fs.writeFileSync(`../public/output/4-top10EconomicalBowlersIn2015.json`, JSON.stringify(top10EconomicalBowlers, null, 2));
        }).catch(error => {
            console.error('Error:', error);
        });
    }).catch(error => {
        console.error('Error:', error);
    });
}

const year = '2015';

top10EconomicalBowlers(year);