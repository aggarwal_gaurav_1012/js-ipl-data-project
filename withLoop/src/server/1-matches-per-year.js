const csvToJSON = require('../util');
const fs = require('fs');

const matchesFilePath = '../data/matches.csv';

function matchesPerYear() {
    csvToJSON(matchesFilePath)
        .then(function(data) {
            const matchesPerYear = {};

            for (let i = 0; i < data.length; i++) {
                const entry = data[i];
                const year = entry['season'];
                matchesPerYear[year] = (matchesPerYear[year] || 0) + 1;
            }

            console.log("File executed");
            fs.writeFileSync("../public/output/1-matchesPerYear.json", JSON.stringify(matchesPerYear, null, 2));
        })
        .catch(function(error) {
            console.error('Error:', error);
        });
}
matchesPerYear();