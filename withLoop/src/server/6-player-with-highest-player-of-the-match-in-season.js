const csvToJSON = require('../util');
const fs = require('fs');

const matchesFilePath = '../data/matches.csv';

function findPlayerOfTheMatchForEachSeason() {
    csvToJSON(matchesFilePath).then((matchesData) => {

        const playerOfTheMatchPerSeason = {};

        for (let i = 0; i < matchesData.length; i++) {
            const match = matchesData[i];
            const season = match['season'];
            const playerOfTheMatch = match['player_of_match'];

            if (!playerOfTheMatchPerSeason[season]) {
                playerOfTheMatchPerSeason[season] = { player: '', count: 0 };
            }

            if (playerOfTheMatch) {
                let count = 0;
                for (let j = 0; j < matchesData.length; j++) {
                    if (matchesData[j]['season'] === season && matchesData[j]['player_of_match'] === playerOfTheMatch) {
                        count++;
                    }
                }

                if (count > playerOfTheMatchPerSeason[season].count) {
                    playerOfTheMatchPerSeason[season] = { player: playerOfTheMatch, count: count };
                }
            }
        }

        console.log("File executed");
        fs.writeFileSync("../public/output/6-playerOfTheMatchPerSeason.json", JSON.stringify(playerOfTheMatchPerSeason, null, 2));
    }).catch(error => {
        console.error('Error:', error);
    });
}

findPlayerOfTheMatchForEachSeason();