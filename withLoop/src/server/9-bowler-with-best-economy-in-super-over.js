const util = require('../util');
const fs = require('fs');

const matchesFilePath = '../data/matches.csv';
const deliveriesFilePath = '../data/deliveries.csv';

function findBestEconomyBowler() {
    util(matchesFilePath).then((matchesData) => {
        util(deliveriesFilePath).then((deliveriesData) => {
            const economyMap = {};
            
            const superOverDeliveries = [];

            for (let i = 0; i < deliveriesData.length; i++) {
                const delivery = deliveriesData[i];
                if (delivery.is_super_over === '1') {
                    superOverDeliveries.push(delivery);
                }
            }

            for (let i = 0; i < superOverDeliveries.length; i++) {
                const delivery = superOverDeliveries[i];
                const bowler = delivery.bowler;
                const runs = parseInt(delivery.total_runs);
                economyMap[bowler] = economyMap[bowler] ? economyMap[bowler] + runs : runs;
            }

            let bestEconomyBowler = '';
            let bestEconomy = Infinity;

            for (const bowler in economyMap) {
                const balls = superOverDeliveries.filter(delivery => delivery.bowler === bowler).length;
                const economy = economyMap[bowler] / (balls / 6);
                if (economy < bestEconomy) {
                    bestEconomy = economy;
                    bestEconomyBowler = bowler;
                }
            }

            console.log("File executed");

            fs.writeFileSync("../public/output/9-bestEconomyBowlerInSuperOver.json", JSON.stringify(bestEconomyBowler, null, 2));

        }).catch(error => {
            console.error('Error reading deliveries data:', error);
        });
    }).catch(error => {
        console.error('Error reading matches data:', error);
    });
}

findBestEconomyBowler();