const csvToJSON = require('../util');
const fs = require('fs');

const matchesFilePath = '../data/matches.csv';
const deliveriesFilePath = '../data/deliveries.csv';

function calculateStrikeRate() {
    csvToJSON(deliveriesFilePath).then((deliveriesData) => {
        csvToJSON(matchesFilePath).then((matchesData) => {
            const strikeRates = {};

            for (let i = 0; i < deliveriesData.length; i++) {
                const delivery = deliveriesData[i];
                const batsman = delivery['batsman'];
                const season = matchesData.find(match => match['id'] === delivery['match_id'])['season'];
                const runs = parseInt(delivery['batsman_runs']);
                const balls = 1;

                if (strikeRates[batsman] === undefined) {
                    strikeRates[batsman] = {};
                }

                if (strikeRates[batsman][season] === undefined) {
                    strikeRates[batsman][season] = { runs: 0, balls: 0 };
                }

                strikeRates[batsman][season]['runs'] += runs;
                strikeRates[batsman][season]['balls'] += balls;
            }

            const output = {};

            for (const batsman in strikeRates) {
                output[batsman] = {};
                for (const season in strikeRates[batsman]) {
                    const runs = strikeRates[batsman][season]['runs'];
                    const balls = strikeRates[batsman][season]['balls'];
                    const strikeRate = (runs / balls) * 100 || 0;
                    output[batsman][season] = strikeRate.toFixed(2);
                }
            }

            console.log("File executed");
            fs.writeFileSync("../public/output/7-strikeRateOfBatsmanForEachSeason.json", JSON.stringify(output, null, 2));
        }).catch(error => {
            console.error('Error:', error);
        });
    }).catch(error => {
        console.error('Error:', error);
    });
}

calculateStrikeRate();