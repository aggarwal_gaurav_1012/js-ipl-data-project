const csv = require('csvtojson');

function csvToJSON(csvFilePath) {
    return new Promise((resolve, reject) => {
        csv()
            .fromFile(csvFilePath)
            .then((jsonArrayObj) => {
                resolve(jsonArrayObj);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

module.exports = csvToJSON;